package com.example.bottomsup.model

import com.example.bottomsup.model.remote.BottomsUpService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AppModule {

    private val BASE_URL = "https://c.thecocktaildb.com"

    @Provides
    @Singleton
    fun provideInstance(): BottomsUpService {
        return Retrofit.Builder().baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build().let{ it.create(BottomsUpService::class.java)}
    }

}