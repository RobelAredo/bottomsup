package com.example.bottomsup.model

import com.example.bottomsup.model.remote.BottomsUpService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class BottomsUpRepo @Inject constructor(val service: BottomsUpService) {

    private val bottomsUpService by lazy { service }

    suspend fun getCategories() = withContext(Dispatchers.IO) {
        bottomsUpService.getCategories().categoryItem.map{it.strCategory}
    }

    suspend fun getDrinks(category: String) = withContext(Dispatchers.IO) {
        bottomsUpService.getDrinksInCategory(category = category).drinks
    }

    suspend fun getDrinkDetails(drinkId: Int) = withContext(Dispatchers.IO) {
        bottomsUpService.getDrinkDetails(drinkId).drinks
    }
}