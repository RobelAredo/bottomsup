package com.example.bottomsup.model

import javax.inject.Inject


class Repo constructor(private val input: Any)

class RepoFactory @Inject constructor() {
    lateinit var input: Any

    fun addInput(newInput: Any) {
        input = newInput
    }

    fun create(): Repo = Repo(input)
}

