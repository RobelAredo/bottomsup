package com.example.bottomsup.model.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding

class GenericAdaptor<A, B, VH : ViewBinding>(
    private val list: List<A>,
    private val load: (A, nav: (B) -> Unit, VH) -> Unit,
    private val bindingInflater: (ViewGroup) -> VH,
    private val navigate: (B) -> Unit = {}
) : RecyclerView.Adapter<GenericAdaptor.GenericViewHolder<A, B, VH>>() {

    private var dataList = mutableListOf<A>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        GenericViewHolder(bindingInflater(parent), load)

    override fun onBindViewHolder(holder: GenericViewHolder<A, B, VH>, position: Int) {
        val data = dataList[position]
        val vH = holder.vH()
        holder.loadData(data, navigate, vH)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    fun add() {
        dataList = list.toMutableList()
    }

    class GenericViewHolder<A, B, VH : ViewBinding>(
        private val binding: VH, private val load: (A, nav: (B) -> Unit, VH) -> Unit
    ) : RecyclerView.ViewHolder(binding.root) {
        fun loadData(data: A, nav: (B) -> Unit, vh: VH) {
            load(data, nav, vh)
        }

        fun vH (): VH {
            return binding
        }
    }
}