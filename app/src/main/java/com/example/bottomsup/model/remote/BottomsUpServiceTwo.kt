package com.example.bottomsup.model.remote

import com.example.bottomsup.model.response.CategoryDTO
import com.example.bottomsup.model.response.CategoryDrinksDTO
import com.example.bottomsup.model.response.DrinkDetailsDTO
import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

abstract class BottomsUpServiceTwo {

    val client = OkHttpClient()
    val url = "$BASE_URL/api/json/v1/1/list.php?c=list"
    val request = Request.Builder().url(url).build()


    companion object {
        private const val BASE_URL = "https://www.thecocktaildb.com"

        fun getInstance(): BottomsUpServiceTwo {
            val retrofit: Retrofit = Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

            return retrofit.create(BottomsUpServiceTwo::class.java)
        }
    }

    //@GET("/api/json/v1/1/list.php?c=list")
//    abstract suspend fun getCategories() {
//
//    }

    //@GET("/api/json/v1/1/filter.php?c=")
    abstract suspend fun getDrinksInCategory(@Query("c") category: String = "Ordinary_Drink") : CategoryDrinksDTO

    //@GET("/api/json/v1/1/lookup.php?i=")
    abstract suspend fun getDrinkDetails(drinkId: Int = 1): DrinkDetailsDTO
}