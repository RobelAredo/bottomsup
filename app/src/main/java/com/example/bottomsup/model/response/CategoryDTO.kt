package com.example.bottomsup.model.response

import com.google.gson.annotations.SerializedName


data class CategoryDTO(
    @SerializedName("drinks")
    val categoryItem: List<CategoryItem>
) {
    data class CategoryItem(
        val strCategory: String
    )
}