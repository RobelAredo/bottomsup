package com.example.bottomsup.model.response


data class CategoryDrinksDTO(
    val drinks: List<Drink>
) {
    data class Drink(
        val idDrink: String,
        val strDrink: String,
        val strDrinkThumb: String
    )
}