package com.example.bottomsup.view.category

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.bottomsup.databinding.FragmentCategoryBinding
import com.example.bottomsup.databinding.ItemBinding
import com.example.bottomsup.model.adapter.GenericAdaptor
import com.example.bottomsup.viewmodel.CategoryViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CategoryFragment: Fragment() {
    private var _binding: FragmentCategoryBinding? = null
    private val binding get() = _binding!!

    private val categoryViewModel by viewModels<CategoryViewModel>()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentCategoryBinding.inflate(inflater, container, false).also {
            _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        categoryViewModel.state.observe(viewLifecycleOwner){ categories ->
            binding.piLoadingCategories.isVisible = categories.isLoading
            binding.rvCategories.adapter =
                GenericAdaptor(categories.categories,
                    ::loadCategory, ::itemInflater, ::navigate).apply { add() }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    private fun navigate(category: String) {
        findNavController().navigate(CategoryFragmentDirections.actionCategoryFragmentToDrinksFragment(category))
    }

    private fun loadCategory(category: String, navigate: (String) -> Unit, binding: ItemBinding) {
            binding.tvItem.text = category
            binding.tvItem.setOnClickListener {
                navigate(category)
            }
    }

    private fun itemInflater (parent: ViewGroup): ItemBinding {
        return ItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    }
}