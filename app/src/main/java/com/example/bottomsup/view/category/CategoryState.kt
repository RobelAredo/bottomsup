package com.example.bottomsup.view.category

data class CategoryState<T> (
    val isLoading: Boolean = false,
    val categories: List<T> = emptyList()
)
