package com.example.bottomsup.view.category

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.example.bottomsup.databinding.FragmentDrinkDetailsBinding
import com.example.bottomsup.databinding.ItemDrinkBinding
import com.example.bottomsup.model.BottomsUpRepo
import com.example.bottomsup.model.adapter.GenericAdaptor
import com.example.bottomsup.model.response.DrinkDetailsDTO
import com.example.bottomsup.viewmodel.DrinkDetailsViewModel
import com.example.bottomsup.viewmodel.DrinksViewModel
import com.example.bottomsup.viewmodel.GenericViewModelFactory
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class DrinkDetailsFragment: Fragment() {
    private var _binding: FragmentDrinkDetailsBinding? = null
    private val binding get() = _binding!!

    private val drinkDetailsViewModel by viewModels<DrinkDetailsViewModel>()
//    private val drinksViewModel by viewModels<DrinksViewModel>()
    private val args by navArgs<DrinkDetailsFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentDrinkDetailsBinding.inflate(inflater, container, false).also {
            _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        drinkDetailsViewModel.state.observe(viewLifecycleOwner){ drinkDetails ->
            binding.piLoadingCategories.isVisible = drinkDetails.isLoading
            binding.rvDrinks.adapter =
                GenericAdaptor(drinkDetails.categories,
                    ::loadDrinkData, ::itemInflater).apply { add() }
        }

//        Log.d("LOGGER", drinksViewModel.state.toString())
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    private fun loadDrinkData(drink: DrinkDetailsDTO.Drink, navigate: (Nothing) -> Unit, binding: ItemDrinkBinding) {
        binding.tvDrinkId.text = drink.strDrink
        binding.tvDrink.text = drink.strInstructions
        Picasso.get().load(drink.strDrinkThumb).into(binding.ivDrinkThumb)
    }

    private fun itemInflater (parent: ViewGroup): ItemDrinkBinding {
        return ItemDrinkBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    }
}