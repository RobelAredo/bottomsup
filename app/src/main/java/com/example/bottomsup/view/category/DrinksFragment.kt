package com.example.bottomsup.view.category

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.bottomsup.databinding.FragmentDrinksBinding
import com.example.bottomsup.databinding.ItemDrinkBinding
import com.example.bottomsup.model.adapter.GenericAdaptor
import com.example.bottomsup.model.response.CategoryDrinksDTO
import com.example.bottomsup.viewmodel.DrinksViewModel
import com.example.bottomsup.viewmodel.GenericViewModelFactory
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class DrinksFragment: Fragment() {
    private var _binding: FragmentDrinksBinding? = null
    private val binding get() = _binding!!

    private val drinksViewModel by viewModels<DrinksViewModel>()
    val args by navArgs<DrinksFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentDrinksBinding.inflate(inflater, container, false).also {
            _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        drinksViewModel.state.observe(viewLifecycleOwner){ drinks ->
            binding.piLoadingCategories.isVisible = drinks.isLoading
            binding.rvDrinks.adapter =
                GenericAdaptor(drinks.categories,
                    ::loadDrinkData, ::itemInflater, ::navigate).apply { add() }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    private fun navigate(id: Int) {
        findNavController().navigate(DrinksFragmentDirections.actionDrinksFragmentToDrinkDetailsFragment(id))
    }

    private fun loadDrinkData(drink: CategoryDrinksDTO.Drink, navigate: (Int) -> Unit, binding: ItemDrinkBinding) {
        binding.tvDrinkId.text = drink.idDrink
        binding.tvDrink.text = drink.strDrink
        Picasso.get().load(drink.strDrinkThumb).into(binding.ivDrinkThumb)
        binding.cvDrinkDetails.setOnClickListener{
            navigate(drink.idDrink.toInt())
        }
    }

    private fun itemInflater (parent: ViewGroup): ItemDrinkBinding {
        return ItemDrinkBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    }
}