package com.example.bottomsup.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.bottomsup.model.BottomsUpRepo
import com.example.bottomsup.model.response.CategoryDTO
import com.example.bottomsup.view.category.CategoryState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CategoryViewModel @Inject constructor(val repo: BottomsUpRepo): ViewModel() {

    private val _state = MutableLiveData<CategoryState<String>>(CategoryState((true)))
    val state: LiveData<CategoryState<String>> get() = _state

    init {
        viewModelScope.launch {
            val categoryDTO = repo.getCategories()
            _state.value = CategoryState(categories = categoryDTO)
        }
    }
}