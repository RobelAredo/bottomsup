package com.example.bottomsup.viewmodel

import android.util.Log
import androidx.lifecycle.*
import com.example.bottomsup.model.BottomsUpRepo
import com.example.bottomsup.model.response.CategoryDTO
import com.example.bottomsup.model.response.CategoryDrinksDTO
import com.example.bottomsup.model.response.DrinkDetailsDTO
import com.example.bottomsup.view.category.CategoryState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DrinkDetailsViewModel @Inject constructor(val repo: BottomsUpRepo, val savedStateHandle: SavedStateHandle): ViewModel() {


    private val _state = MutableLiveData<CategoryState<DrinkDetailsDTO.Drink>>(CategoryState((true)))
    val state: LiveData<CategoryState<DrinkDetailsDTO.Drink>> get() = _state

    init {
        viewModelScope.launch {
            val drinkDetailsDTO = repo.getDrinkDetails(savedStateHandle["drinkId"]!!)
            _state.value = CategoryState(categories = drinkDetailsDTO)
        }
    }
}