package com.example.bottomsup.viewmodel

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.*
import com.example.bottomsup.R
import com.example.bottomsup.databinding.ItemBinding
import com.example.bottomsup.model.BottomsUpRepo
import com.example.bottomsup.model.response.CategoryDrinksDTO
import com.example.bottomsup.view.category.CategoryState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DrinksViewModel @Inject constructor(val repo: BottomsUpRepo, val savedStateHandle: SavedStateHandle) : ViewModel() {

    var category: String = savedStateHandle["category"]!!
    private val _state =
        MutableLiveData<CategoryState<CategoryDrinksDTO.Drink>>(CategoryState((true)))
    val state: LiveData<CategoryState<CategoryDrinksDTO.Drink>> get() = _state

    init {
        viewModelScope.launch {
            val drinksDTO = repo.getDrinks(category)
            _state.value = CategoryState(categories = drinksDTO)
        }
    }
}