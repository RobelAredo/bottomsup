package com.example.bottomsup.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class GenericViewModelFactory(
    private val viewModel: ViewModel
): ViewModelProvider.NewInstanceFactory() {

    override fun <T:ViewModel> create(modelClass: Class<T>): T =
        viewModel as T
}